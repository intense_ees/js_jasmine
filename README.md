# Local Day of Coderetreat 2023 #

### Vorbereitungen und Infos für den LDCR ###

* Installation Visual Studio Code: https://code.visualstudio.com/download
* Die Doku für das Unit Test Framework Jasmine überfliegen: https://jasmine.github.io/tutorials/your_first_suite
* Mit der grundlegenden Nutzung von **GIT** vertraut machen, besonders innerhalb von Visual Studio Code: https://code.visualstudio.com/docs/editor/versioncontrol#_cloning-a-repository

* Dieses Repository clonen

* Das Unit Test Framework Jasmine ist bereits in diesem Repo enthalten und muss daher nicht zusätzlich installiert werden
* Reines JavaScript, kein Node.js oder ähnliches nötig
* Die benötigten Jasmine Tests sind sehr einfach und ein Beispiel ist bereits im Repo enthalten

### Enthaltene Dateien und Ordner ###

* Ordner **spec**: Enthält die Unit Test Dateien
	* **spec/tcg_spec.js**: Hier baut ihr eure Tests
	* **spec/spec_helper.js**: Einfach **ignorieren**, wird von Jasmine benötigt
* Ordner **src**: Enthält den eigentlichen Code, hier könnt ihr euch austoben
	* **src/tcg.js**: Der Ursprung allen Elends :)
* **SpecRunner.html**: Diese Datei einfach im **Browser** öffnen zur Ausführung der Unit Tests

### Vorgehensweise für Coderetreat ###

* **pull**
* **change**
* **commit&push**
* **repeat**

### Tipps für den Anfang ###

* Die Browser Console (in Chrome über **F12** erreichbar) zeigt den Output des aktuellen Codes.
* Mit der Tastenkombination **CRTL+SHIFT+R** könnt ihr die refactoring Funktion von Visual Studio Code nutzen. (**CMD+R** auf Mac)

### Beschreibung des Katas ###

**Trading Card Game**

A game for two players in which you play cards in turns to bring your enemy`s health to zero.

**Preparation**
* Each player starts the game with 30 Health and 0 Mana slots
* Each player starts with a deck of 20 Damage cards with the following Mana costs: 0,0,0,1,1,2,2,2,3,3,3,4,4,4,5,5,6,6,7,8
* From the deck each player receives 3 random cards has his initial hand

**Gameplay**
* The active player receives 1 Mana slot up to a maximum of 10 total slots
* The active player’s empty Mana slots are refilled
* The active player draws a random card from his deck
* The active player can play as many cards as he can afford. Any played card empties Mana slots and deals immediate damage to the opponent player equal to its Mana cost.
* If the opponent player’s Health drops to or below zero the active player wins the game
* If the active player can’t (by either having no cards left in his hand or lacking sufficient Mana to pay for any hand card) or simply doesn’t want to play another card, the opponent player becomes active

**Special Rules** 
* Bleeding Out: If a player’s card deck is empty before the game is over he receives 1 damage instead of drawing a card when it’s his turn.
* Overload: If a player draws a card that lets his hand size become >5 that card is discarded instead of being put into his hand.
* Curse: The 0 Mana cards can be played for free to curse the enemy. An accursed Player may only play one card per turn. A curse can be lifted by playing a 0 Mana card by the accursed player.
* Healing: A player can decide to heal himself by a played card`s value instead of dealing damage with it. A Player`s health is always limited to 30 points.
