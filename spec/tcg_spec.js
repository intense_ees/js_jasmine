describe("Trading Card Game", function () {
  it("should do something", function () {
    let game = new Game(1, 2);
    let a = game.machWas();

    let expectedResult = 3;
    let wrongResult = 2;
    expect(a).toBe(expectedResult);
    expect(a).not.toBe(wrongResult);
  });
});
